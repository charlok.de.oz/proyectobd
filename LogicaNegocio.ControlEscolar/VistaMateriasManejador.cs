﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class VistaMateriasManejador
    {
        private VistaMateriasAccesoDatos _vistaMateriasAccesoDatos = new VistaMateriasAccesoDatos();
        public List<VistaMaterias> GetVistas(string filtro)
        {
            var listMaterias = _vistaMateriasAccesoDatos.GetVistaMaterias(filtro);
            return listMaterias;
        }
    }
}
