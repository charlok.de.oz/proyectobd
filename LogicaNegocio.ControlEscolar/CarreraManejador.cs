﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class CarreraManejador
    {
        private CarreraAccesoDatos _carreraAccesoDatos = new CarreraAccesoDatos();
        public void GuardarCarrera(Carreras carreras,int x)
        {
            _carreraAccesoDatos.Guardar(carreras,x);
        }
        public void EliminarCarrera(string nombre)
        {
            _carreraAccesoDatos.eliminar(nombre);
        }
        public List<Carreras> getCarreras(string filtro)
        {
            var listCarreras = _carreraAccesoDatos.GetCarreras(filtro);
            return listCarreras;
        }
        public DataTable dtMaestro(DataTable dtt2)
        {
            var dtt = _carreraAccesoDatos.dtMaestros(dtt2);
            return dtt;
        }
    }
}
