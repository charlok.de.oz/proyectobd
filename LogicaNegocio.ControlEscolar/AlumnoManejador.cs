﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
     public class AlumnoManejador
    {
        //////////////////////////////////////////////////////////////////////////
        private AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();
        public void GuardarAlumno(Alumno alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);
        }
        public void EliminarAlumno(int nControl)
        {
            _alumnoAccesoDatos.eliminar(nControl);
        }
        public List<Alumno> GetAlumnos(string filtro)
        {
            var listAlumno = _alumnoAccesoDatos.GetAlumnos(filtro);
            return listAlumno;
        }
    }
}
