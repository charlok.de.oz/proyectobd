﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _materiasAccesoDatos = new MateriasAccesoDatos();
        public void GuardarMateria(Materias materias, int x)
        {
            _materiasAccesoDatos.Guardar(materias,x);
        }
        public void EliminarMateria(string nombre)
        {
            _materiasAccesoDatos.eliminar(nombre);
        }
        public List<Materias> getMaterias(string filtro)
        {
            var listMaterias = _materiasAccesoDatos.getMaterias(filtro);
            return listMaterias;
        }
        public DataTable dtMaterias(DataTable dtt2)
        {
            var dtt = _materiasAccesoDatos.dtMaterias(dtt2);
            return dtt;
        }
    }
}
