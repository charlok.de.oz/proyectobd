﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _usuarioAccesoDatos = new UsuarioAccesoDatos();
        public void Guardar(Usuario usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);
        }
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+([A-Za-z]+)*$");
            var match = regex.Match(nombre);
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        public Tuple<bool,string>ValidarUsuarii(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;
            if (usuario.Nombre.Length == 0)
            {
                mensaje = mensaje + "El nombre de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.Nombre.Length > 20)
            {
                mensaje = mensaje + "El nombre de usuario solo permite un máximo de 20 caracteres \n";
            }

            if (usuario.ApellidoPaterno.Length == 0)
            {
                mensaje = mensaje + "El apelllido parterno es necesario \n";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length > 20)
            {
                mensaje = mensaje + "El apellido paterno de usuario solo permite un máximo de 20 caracteres \n";
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre de usuario";
            }
            return Tuple.Create(valido, mensaje);
        }
    
        public void eliminar(int idUsuario)
        {
            _usuarioAccesoDatos.eliminar(idUsuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
           
            var listUsuario = _usuarioAccesoDatos.GetUsuarios(filtro);
            
            //Llenar lista
            return listUsuario;
        }

        
        
        ///////////////////////////////////////////////////////////////////////////77

    }
}
