﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GrupoManejador
    {
        private GruposAccesoDatos _gruposAccesoDatos = new GruposAccesoDatos();
        public void GuardarGrupo(Grupos grupos)
        {
            _gruposAccesoDatos.Guardar(grupos);
        }
        public void EliminarGrupo(int idGrupo)
        {
            _gruposAccesoDatos.eliminar(idGrupo);
        }
        public List<Grupos> GetGrupos(string filtro)
        {
            var listGrupo = _gruposAccesoDatos.GetGrupos(filtro);
            return listGrupo;
        }
    }
}
