﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class EstadoManejador
    {
        ///////////////////////////////////////////////////////////////////////////77
        private EstadoAccesoDatos _estadoAccesoDatos = new EstadoAccesoDatos();

        public List<Estados> GetEstados(string filtro)
        {
            
            var listEstados = _estadoAccesoDatos.GetEstados(filtro);
            return listEstados;
        }
        public DataTable dtEstado(DataTable dtt2)
        {
            var dtt = _estadoAccesoDatos.dtEstado(dtt2);
            return dtt;
        }
        public DataTable idEstado(string filtro)
        {
            var dtt = _estadoAccesoDatos.idEstado(filtro);
            return dtt;
        }
        public string dtEstadoid2(string filtro)
        {
            string result = _estadoAccesoDatos.dtEstadoID(filtro);
            return result;
        }
        /*public List<Estados>getisd(string filtro)
        {
            var listIds = _estadoAccesoDatos.dtEstadoID(filtro);
            return listIds;
        }*/
    }
}
