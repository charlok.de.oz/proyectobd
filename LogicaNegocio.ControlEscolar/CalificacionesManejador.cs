﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CalificacionesManejador
    {
        private CalificacionAccesoDatos _calificacionAccesoDatos = new CalificacionAccesoDatos();
        public void GuardarCalificacion(Calificacion calificacion)
        {
            _calificacionAccesoDatos.Guardar(calificacion);
        }
        public void EliminarCalificacion(int idCalif)
        {
            _calificacionAccesoDatos.eliminar(idCalif);
        }
        public List<Calificacion> GetCalificacions(string filtro)
        {
            var listCalificaciones = _calificacionAccesoDatos.GetCalificacion(filtro);
            return listCalificaciones;
        }
    }
}
