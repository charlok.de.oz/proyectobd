﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class RelacionManejador
    {
        private RelacionAccesoDatos _relacionAccesoDatos = new RelacionAccesoDatos();
        public void GuardarRelacion(Relacion relacion, int x)
        {
            _relacionAccesoDatos.Guardar(relacion, x);
        }
        public void EliminarRelacion(string nombre)
        {
            _relacionAccesoDatos.eliminar(nombre);
        }
        public List<Relacion> getRelacion(string filtro)
        {
            var listRelacion = _relacionAccesoDatos.getRelacion(filtro);
            return listRelacion;
        }
    }
}
