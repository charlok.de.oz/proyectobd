﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class MaestroManejador
    {
        private MaestroAccesoDatos _maestroAccesoDatos = new MaestroAccesoDatos();
        public void GuardarMaestro(Maestro maestro,int x)
        {
            _maestroAccesoDatos.Guardar(maestro,x);
        }
        public void ActualizarMaestro(Maestro maestro)
        {
            _maestroAccesoDatos.Actualizar(maestro);
        }
        public void EliminarMaestro(string nombre)
        {
            _maestroAccesoDatos.eliminar(nombre);
        }
        public List<Maestro> GetMaestro(string filtro)
        {
            var listMaestros = _maestroAccesoDatos.GetMaestros(filtro);
            return listMaestros;
        }
        public DataTable idMaestro(string filtro)
        {
            var dtt = _maestroAccesoDatos.idMaestro(filtro);
            return dtt;
        }
        public int idMaestro2(string filtro)
        {
            var dtt = _maestroAccesoDatos.idMaestro(filtro);
            int xdxd;
            xdxd = int.Parse(dtt.ToString());
            return xdxd;
        }
    }
}
