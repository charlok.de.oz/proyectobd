﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class MunicipioManejador
    {
        private MunicipioAccesoDatos _municipioAccesoDatos = new MunicipioAccesoDatos();
        public List<Municipios> GetMunicipios(string filtro)
        {
            var listMunicipios = _municipioAccesoDatos.GetMunicipios(filtro);
            return listMunicipios;
        }
        public DataTable dtMunicipio(string filtro)
        {
            var dtt = _municipioAccesoDatos.dtMunicipios(filtro);
            return dtt;
        }
    }
}
