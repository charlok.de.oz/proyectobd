﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class VistaManejador
    {
        private VistaAccesoDatos _vistaAccesoDatos = new VistaAccesoDatos();
        public List<Vista> GetVista(string filtro)
        {
            var listCarrera = _vistaAccesoDatos.GetVista(filtro);
            return listCarrera;
        }
    }
}
