﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GruposAlumnosManejador
    {
        private GruposAlumnosAccesoDatos _gruposAlumnosAccesoDatos = new GruposAlumnosAccesoDatos();
        public void GuardarGrupoA(GruposAlumnos gruposAlumnos)
        {
            _gruposAlumnosAccesoDatos.Guardar(gruposAlumnos);
        }
        public void EliminarGrupoA(int idGruposAl)
        {
            _gruposAlumnosAccesoDatos.eliminar(idGruposAl);
        }
        public List<GruposAlumnos> GetGruposAlumnos(string filtro)
        {
            var listGruposA = _gruposAlumnosAccesoDatos.GetGruposAlumnos(filtro);
            return listGruposA;
        }
    }
}
