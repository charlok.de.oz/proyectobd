﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();
        public void GuardarEscuela(Escuela escuela, int x)
        {
            _escuelaAccesoDatos.Guardar(escuela, x);
        }
        public void EliminarEscuela(string nombre)
        {
            _escuelaAccesoDatos.eliminar(nombre);
        }
        public List<Escuela> GetEscuelas(string filtro)
        {
            var listEscuela = _escuelaAccesoDatos.GetEscuelas(filtro);
            return listEscuela;
        }
        public DataSet Mostrar()
        {
            DataSet dsc = _escuelaAccesoDatos.consulta();
            return dsc;
        }
    }
}
