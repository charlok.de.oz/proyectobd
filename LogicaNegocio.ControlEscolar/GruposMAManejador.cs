﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GruposMAManejador
    {
        private GruposMAAccesoDatos _gruposMAAccesoDatos = new GruposMAAccesoDatos();
        public void GuardarGrupoMA(GruposMA gruposMA)
        {
            _gruposMAAccesoDatos.Guardar(gruposMA);
        }
        public void EliminarGrupoMA(int idGrupoMA)
        {
            _gruposMAAccesoDatos.eliminar(idGrupoMA);
        }
        public List<GruposMA> GetGruposMA(string filtro)
        {
            var listGruposMA = _gruposMAAccesoDatos.GetGruposMA(filtro);
            return listGruposMA;
        }
    }
}
