﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CalificacionVistaManejador
    {
        private CalificacionVistaAccesoDatos _calificacionVistaAccesoDatos = new CalificacionVistaAccesoDatos();
        public List<CalificacionVista> GetCalificacionVistas(string filtro)
        {
            var listCalificacionVista = _calificacionVistaAccesoDatos.GetCalificacionVistas(filtro);
            return listCalificacionVista;
        }
    }
}
