﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class Escuela : Form
    {
        private EscuelaManejador _escuelaManejador;
        private OpenFileDialog _imagen;
        DataSet ds;
        private string _ruta;
        public Escuela()
        {
           
            InitializeComponent();
            _escuelaManejador = new EscuelaManejador();
            _imagen = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\logo\\";
            btnImportar.Enabled = false;
            btnBorrar.Enabled = false;
        }
        //---------------------------------------------------------
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar,bool importar,bool borrar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
            btnImportar.Enabled = importar;
            btnBorrar.Enabled = borrar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtEncargado.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtCorreo.Enabled = activar;
            txtPagina.Enabled = activar;
            txtRFC.Enabled = activar;
            txtTelefono.Enabled = activar;
            pcbLogo.Enabled = activar;
            txtRuta.Enabled = activar;
        }
        public void BorrarArchivo(String archivo)
        {
            if (File.Exists(@archivo))
            {
                try
                {
                    File.Delete(@archivo);
                }
                catch (Exception e)
                {

                }

            }
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtEncargado.Text = "";
            txtDomicilio.Text = "";
            txtCorreo.Text = "";
            txtPagina.Text = "";
            lblX.Text = "0";
            txtRFC.Text = "";
            txtTelefono.Text = "";
            //pcbLogo.Image = null;
            txtRuta.Text = "";
        }
        //---------------------------------------------------------
        private void buscarEscuela(string filtro)
        {
            dgvEscuela.DataSource = _escuelaManejador.GetEscuelas(filtro);
        }
        private void buscarEscuela2()
        {
            ds = _escuelaManejador.Mostrar();
            if (ds.Tables[0].Rows.Count == 0)
            {

            }
            else
            {
                try
                {
                    txtID.Text = ds.Tables[0].Rows[0]["idEsc"].ToString();
                    txtNombre.Text= ds.Tables[0].Rows[0]["nombreEsc"].ToString();
                    txtRFC.Text= ds.Tables[0].Rows[0]["rfcEsc"].ToString();
                    txtDomicilio.Text= ds.Tables[0].Rows[0]["domicilioEsc"].ToString();
                    txtTelefono.Text= ds.Tables[0].Rows[0]["telefonoEsc"].ToString();
                    txtCorreo.Text= ds.Tables[0].Rows[0]["correoEsc"].ToString();
                    txtPagina.Text= ds.Tables[0].Rows[0]["pagina"].ToString();
                    txtEncargado.Text= ds.Tables[0].Rows[0]["nombreEncargado"].ToString();
                    txtRuta.Text= ds.Tables[0].Rows[0]["LogoEsc"].ToString();
                    txtRuta2.Text= ds.Tables[0].Rows[0]["LogoEsc"].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarEscuela()
        {
            //var nombre = dgvEscuela.CurrentRow.Cells["nombreEsc"].Value;
            //_escuelaManejador.EliminarEscuela(nombre.ToString());
            _escuelaManejador.EliminarEscuela(txtNombre.Text);
        }
        private void GuardarEscuela()
        {
            _escuelaManejador.GuardarEscuela(new Entidades.ControlEscolar.Escuela
            {

                IDEscuela = int.Parse("1"),
                NombreEsc = txtNombre.Text,
                RFCEsc = txtRFC.Text,
                DomicilioEsc = txtDomicilio.Text,
                CorreoEsc =txtCorreo.Text,
                EncargadoEsc =txtEncargado.Text,
                LogoEsc =txtRuta.Text,
                PaginaEsc=txtPagina.Text,
                TelefonoEsc =int.Parse(txtTelefono.Text),
            }, int.Parse(lblX.Text));
        }
        private void ModificarEscuela()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false,true,true);
            txtID.Text = dgvEscuela.CurrentRow.Cells["IDEscuela"].Value.ToString();
            txtNombre.Text = dgvEscuela.CurrentRow.Cells["NombreEsc"].Value.ToString();
            txtRFC.Text = dgvEscuela.CurrentRow.Cells["RFCEsc"].Value.ToString();
            txtDomicilio.Text = dgvEscuela.CurrentRow.Cells["DomicilioEsc"].Value.ToString();
            txtTelefono.Text = dgvEscuela.CurrentRow.Cells["TelefonoEsc"].Value.ToString();
            txtCorreo.Text = dgvEscuela.CurrentRow.Cells["CorreoEsc"].Value.ToString();
            txtPagina.Text = dgvEscuela.CurrentRow.Cells["PaginaEsc"].Value.ToString();
            txtEncargado.Text = dgvEscuela.CurrentRow.Cells["EncargadoEsc"].Value.ToString();
            txtRuta.Text = dgvEscuela.CurrentRow.Cells["LogoEsc"].Value.ToString();
            txtRuta2.Text = dgvEscuela.CurrentRow.Cells["LogoEsc"].Value.ToString();
            pcbLogo.Image = Image.FromFile(_ruta+txtRuta.Text);
        }
        //--------------------------------------------------------
        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void TxtRFC_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //buscarEscuela(txtBuscar.Text);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false,true,true);
            ControlarCuadros(true);
        }

        private void Escuela_Load(object sender, EventArgs e)
        {
            try
            {
                //buscarEscuela("");
                buscarEscuela2();
                //txtID.Text = dgvEscuela.CurrentRow.Cells["IDEscuela"].Value.ToString();
                if (txtID.Text != "")
                {
                   // txtRuta.Text = dgvEscuela.CurrentRow.Cells["LogoEsc"].Value.ToString();
                    pcbLogo.Image = Image.FromFile(_ruta + txtRuta.Text);
                    btnNuevo.Enabled = false;
                    
                }
                else
                {
                    ControlarBotones(true, false, false, true, false, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No hay registro");
            }
           
            ControlarCuadros(false);
           // LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    
                    
                    EliminarJPG();
                    EliminarEscuela();
                    pcbLogo.Image = null;
                    //pcbLogo.Dispose = false;
                    txtRuta.Text = "";
                    Directory.Delete(Application.StartupPath + "\\logo\\", true);
                    buscarEscuela2();
                    ControlarBotones(true, false, false, false, false, false);
                    LimpiarCuadros();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, false, false, true,false,false);
            ControlarCuadros(false);

            try
            {
                if(lblX.Text != "0")
                {
                    //EliminarJPG();
                    //EliminarEscuela();
                    //pcbLogo.Image = null;
                    //Directory.Delete(Application.StartupPath + "\\logo\\", true);
                    //Directory.Delete(Application.StartupPath + "\\logo\\");
                    GuardarImagenJPG();
                    GuardarEscuela();
                    LimpiarCuadros();
                    //buscarEscuela("");
                    buscarEscuela2();
                    lblX.Text = "";
                }
                else
                {
                    //Directory.Delete(Application.StartupPath + "\\logo\\", true);
                    GuardarImagenJPG();
                    GuardarEscuela();
                    LimpiarCuadros();
                    //buscarEscuela("");
                    buscarEscuela2();
                    lblX.Text = "";
                }
                   
                //pcbLogo.Image = Image.FromFile(_ruta + txtRuta.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DgvEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblX.Text = "1";
            try
            {
                ModificarEscuela();
                //pcbLogo.Image = Image.FromFile(_ruta + txtRuta.Text);
                buscarEscuela("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //---------------------------------------------------------------
        private void BtnImportar_Click(object sender, EventArgs e)
        {
            
            cargarImagen();
        }
        private void cargarImagen()
        {
            _imagen.Filter = "Imagen tipo (*.jpg)|*.jpg|(*.png)|*png";
            _imagen.Title = "Cargar Imagen";
            _imagen.ShowDialog();
            
            

            if (_imagen.FileName != "")
            {
                var archivo = new FileInfo(_imagen.FileName);
                if(archivo.Length > 5000000)
                {
                    MessageBox.Show("El archivo es demasiado grande");
                }
                else{
                    txtRuta.Text = archivo.Name;
                    pcbLogo.Image = Image.FromFile(archivo.DirectoryName+"\\"+archivo.Name);
                    //pcbLogo.Image = Image.FromFile(archivo.FullName);
                }
                
            }
        }
        private void GuardarImagenJPG()
        {
            if (_imagen.FileName != null)
            {
                if (_imagen.FileName != "")
                {
                    var archivo = new FileInfo(_imagen.FileName);
                    if (Directory.Exists(_ruta))
                    {
                        MessageBox.Show(_ruta);
                        //Codigo para  ++ el archivo
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //Codigo Para reemplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                BorrarArchivo(archivoAnterior.FullName);
                                //archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }

            }
        }
        private void EliminarJPG()
        {
           // pcbLogo.Image = null;
            if (_imagen.FileName != "")
                {
                    var archivo = new FileInfo(_imagen.FileName);
                    if (Directory.Exists(_ruta))
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                               // BorrarArchivo(archivoAnterior.FullName);
                               archivoAnterior.Delete();
                                
                                txtRuta.Clear();
                            }
                        }
                    }
             }
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
                pcbLogo.Image = null;
                txtRuta.Text = "";
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            //buscarEscuela("");
            buscarEscuela2();
            //txtRuta.Text = dgvEscuela.CurrentRow.Cells["LogoEsc"].Value.ToString();
            pcbLogo.Image = Image.FromFile(_ruta + txtRuta2.Text);
            ControlarCuadros(false);
            //LimpiarCuadros();
        }

        private void DgvEscuela_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            lblX.Text = "1";
            try
            {
                ControlarCuadros(true);
                ControlarBotones(false, true, true, false, true, true);
                //ModificarEscuela();
                //pcbLogo.Image = Image.FromFile(_ruta + txtRuta.Text);
                //buscarEscuela("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
