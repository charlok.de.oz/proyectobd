﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class GrupoAlumnos : Form
    {
        System.Data.DataTable dxd = new System.Data.DataTable();
        private GrupoManejador _grupoManejador;
        private AlumnoManejador _alumnoManejador;
        private GruposAlumnosManejador _gruposAlumnosManejador;
        public GrupoAlumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _grupoManejador = new GrupoManejador();
            _gruposAlumnosManejador = new GruposAlumnosManejador();
        }
        //----------------------------------------------------------------------//
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbAlumnos.Enabled = activar;
            cmbGrupo.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            cmbGrupo.Text = "";
            cmbAlumnos.Text = "";
        }
        //----------------------------------------------------------------------//
        private void BuscarGruposAl(string filtro)
        {
            dgvGruposAl.DataSource = _gruposAlumnosManejador.GetGruposAlumnos(filtro);
        }
        private void EliminarGrupoAl()
        {
            var idGrupo = dgvGruposAl.CurrentRow.Cells["idGruposAlumnos"].Value;
            _gruposAlumnosManejador.EliminarGrupoA(Convert.ToInt32(idGrupo));
        }
        private void GuardarGrupoAl()
        {
            _gruposAlumnosManejador.GuardarGrupoA(new GruposAlumnos
            {
                idGruposAlumnos = Convert.ToInt32(lblX.Text),
                fkGrupo = int.Parse(cmbGrupo.SelectedValue.ToString()),
                fkAlumno = int.Parse(cmbAlumnos.SelectedValue.ToString()),
            }); ;
        }
        private void ModificarGrupoAl()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblX.Text = dgvGruposAl.CurrentRow.Cells["idGruposAlumnos"].Value.ToString();
            cmbGrupo.Text = dgvGruposAl.CurrentRow.Cells["fkGrupo"].Value.ToString();
            cmbAlumnos.Text = dgvGruposAl.CurrentRow.Cells["fkAlumno"].Value.ToString();
        }
        //----------------------------------------------------------------------//
        private void obtenerGrupos(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupos(dxd.ToString());
            cmbGrupo.DisplayMember = "nombreGrupo";
            cmbGrupo.ValueMember = "idGrupo";

        }
        private void obtenerAlumnos(string filtro)
        {
            cmbAlumnos.DataSource = _alumnoManejador.GetAlumnos(dxd.ToString());
            cmbAlumnos.DisplayMember = "nombreal";
            cmbAlumnos.ValueMember = "ncontrol";

        }
        //----------------------------------------------------------------------//
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGruposAl(txtBuscar.Text);
        }

        private void CmbGrupo_Click(object sender, EventArgs e)
        {
            obtenerGrupos("");   
        }

        private void CmbAlumnos_Click(object sender, EventArgs e)
        {
            obtenerAlumnos("");
        }

        private void DgvGruposAl_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void GrupoAlumnos_Load(object sender, EventArgs e)
        {
            BuscarGruposAl("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarGrupoAl();
                    BuscarGruposAl("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            cmbGrupo.Enabled = true;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupoAl();
                LimpiarCuadros();
                lblX.Text = "0";
                BuscarGruposAl("");
                cmbGrupo.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void DgvGruposAl_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cmbAlumnos.Enabled = false;
            try
            {
                ModificarGrupoAl();
                BuscarGruposAl("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
