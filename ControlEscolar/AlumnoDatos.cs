﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;

namespace ControlEscolar
{
    public partial class frmAlumno : Form
    {
        System.Data.DataTable dxd = new System.Data.DataTable();
        private AlumnoManejador _alumnoManejador;
        private MunicipioManejador _municipioManejador;
        private EstadoManejador _estadoManejador;
        public frmAlumno()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _municipioManejador = new MunicipioManejador();
            _estadoManejador = new EstadoManejador();
            dtpFN.Format = DateTimePickerFormat.Custom;
            dtpFN.CustomFormat = "yyyy-MM-dd";
          
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }
        /////////////////////////////////////////////////////
    private void obtenerestados(string filtro)
        {
            cmbEstados.DataSource = _estadoManejador.dtEstado(dxd);
            cmbEstados.DisplayMember = "nombree";
            cmbEstados.ValueMember = "idest";
        }
        private void obtenermunicipios(string filtro)
        {
            cmbMunicipios.DataSource = _municipioManejador.dtMunicipio(cmbEstados.SelectedValue.ToString());
            cmbMunicipios.DisplayMember = "nombremun";
        }
        /////////////
        private void BuscarAlumnos(string filtro)
        {
            dgvAlumno.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }
        private void EliminarAlumnos()
        {
            var nControl = dgvAlumno.CurrentRow.Cells["ncontrol"].Value;
            _alumnoManejador.EliminarAlumno(Convert.ToInt32(nControl));
        }

        private void GuardarAlumnos()
        {
            _alumnoManejador.GuardarAlumno(new Alumno
            {
                NControl = Convert.ToInt32(lblId.Text),
                NombreAl = txtNombre.Text,
                ApellidoPaternoAl = txtApellidop.Text,
                ApellidoMaternoAl = txtApellidom.Text,
                FechaNacimientoAl = dtpFN.Text,
                DomicilioAl = txtDomicilio.Text,
                CorreoAl = txtCorreo.Text,
                SexoAl = cmbSexo.Text,
                Estado = cmbEstados.Text,
                Municipio= cmbMunicipios.Text,
                
            });
        }
        private void ModificarAlumnos()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblId.Text = dgvAlumno.CurrentRow.Cells["ncontrol"].Value.ToString();
            txtNombre.Text = dgvAlumno.CurrentRow.Cells["nombreal"].Value.ToString();
            txtApellidop.Text = dgvAlumno.CurrentRow.Cells["apellidopaternoal"].Value.ToString();
            txtApellidom.Text = dgvAlumno.CurrentRow.Cells["apellidomaternoal"].Value.ToString();
            dtpFN.Text = dgvAlumno.CurrentRow.Cells["fechanacimientoal"].Value.ToString();
            txtDomicilio.Text = dgvAlumno.CurrentRow.Cells["domicilioal"].Value.ToString();
            txtCorreo.Text = dgvAlumno.CurrentRow.Cells["correoal"].Value.ToString();
            cmbSexo.Text = dgvAlumno.CurrentRow.Cells["sexoal"].Value.ToString();
            cmbEstados.Text= dgvAlumno.CurrentRow.Cells["estado"].Value.ToString();
            cmbMunicipios.Text = dgvAlumno.CurrentRow.Cells["municipio"].Value.ToString();
        }
        /////////////////////////////////////////////////////////
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            txtCorreo.Enabled = activar;
            txtDomicilio.Enabled = activar;
            cmbSexo.Enabled = activar;
            dtpFN.Enabled = activar;
            cmbMunicipios.Enabled = activar;
            cmbEstados.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            txtCorreo.Text = "";
            txtDomicilio.Text = "";
            dtpFN.Text = "";
            cmbSexo.Text = "";
            lblId.Text = "0";
            cmbEstados.Text = "";
            cmbMunicipios.Text = "";
        }

        private void FrmAlumno_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarAlumnos();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void DgvAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumnos();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAlumnos();
                LimpiarCuadros();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }

        private void CmbEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            obtenermunicipios("");
        }

        private void CmbEstados_Click(object sender, EventArgs e)
        {
            obtenerestados("");
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            exportarDataGRidViewExcel(dgvAlumno);
        }

        private void exportarDataGRidViewExcel(DataGridView dgv)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el dgv llenando la hoja de trabajo
                    for (int i = 0; i < dgv.Rows.Count; i++)
                    {
                        for (int j = 0; j < dgv.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Falló la creación de archivo, intenta de nuevo con otro nombre", "Error de creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DgvAlumno_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        ////////////////////////////////////////////////////



        ////////////////////////////////////////////////////
    }
}
