﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;

namespace ControlEscolar
{
    public partial class Calificaciones : Form
    {
        System.Data.DataTable dxd = new System.Data.DataTable();
        private GrupoManejador _grupoManejador;
        private MateriasManejador _materiasManejador;
        private AlumnoManejador _alumnoManejador;
        private CalificacionesManejador _calificacionesManejador;
        private CalificacionVistaManejador _calificacionVistaManejador;
        public Calificaciones()
        {
            InitializeComponent();
            _calificacionesManejador = new CalificacionesManejador();
            _grupoManejador = new GrupoManejador();
            _alumnoManejador = new AlumnoManejador();
            _materiasManejador = new MateriasManejador();
            _calificacionVistaManejador = new CalificacionVistaManejador();
        }
        //----------------------------------------------------------------------//
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbMateria.Enabled = activar;
            cmbAlumnos.Enabled = activar;
            cmbGrupo.Enabled = activar;
            txtParcial1.Enabled = activar;
            txtParcial2.Enabled = activar;
            txtParcial3.Enabled = activar;
            txtParcial4.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            cmbGrupo.Text = "";
            cmbAlumnos.Text = "";
            cmbMateria.Text = "";
            txtParcial1.Text = "";
            txtParcial2.Text = "";
            txtParcial3.Text = "";
            txtParcial4.Text = "";
        }
        //----------------------------------------------------------------------//
        private void BuscarCalificaciones(string filtro)
        {
           // dgvCalificaciones.DataSource = _calificacionesManejador.GetCalificacions(filtro);
        }
        private void BuscarVista(string filtro)
        {
            dgvCalificaciones.DataSource = _calificacionVistaManejador.GetCalificacionVistas(filtro);
        }
        private void EliminarCalif()
        {
            var idCalif = dgvCalificaciones.CurrentRow.Cells["idCalificacion"].Value;
            _calificacionesManejador.EliminarCalificacion(Convert.ToInt32(idCalif));
        }
        private void GuardarCalif()
        {
            _calificacionesManejador.GuardarCalificacion(new Calificacion
            {
                idCalificacion = Convert.ToInt32(lblX.Text),
                fkAlumnoC = int.Parse(cmbAlumnos.SelectedValue.ToString()),
                fkGrupoC = int.Parse(cmbGrupo.SelectedValue.ToString()),
                fkMateriaC = cmbMateria.SelectedValue.ToString(),
                parcial1= Convert.ToInt32(txtParcial1.Text),
                parcial2= Convert.ToInt32(txtParcial2.Text),
                parcial3=Convert.ToInt32(txtParcial3.Text),
                parcial4= Convert.ToInt32(txtParcial4.Text),
            }); ;
        }
        private void ModificarCalif()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblX.Text = dgvCalificaciones.CurrentRow.Cells["idCalificacion"].Value.ToString();
            cmbGrupo.Text = dgvCalificaciones.CurrentRow.Cells["fkGrupoC"].Value.ToString();
            cmbAlumnos.Text = dgvCalificaciones.CurrentRow.Cells["fkAlumnoC"].Value.ToString();
            cmbMateria.Text = dgvCalificaciones.CurrentRow.Cells["fkMateriaC"].Value.ToString();
            txtParcial1.Text= dgvCalificaciones.CurrentRow.Cells["parcial1"].Value.ToString();
            txtParcial2.Text = dgvCalificaciones.CurrentRow.Cells["parcial2"].Value.ToString();
            txtParcial3.Text = dgvCalificaciones.CurrentRow.Cells["parcial3"].Value.ToString();
            txtParcial4.Text = dgvCalificaciones.CurrentRow.Cells["parcial4"].Value.ToString();
        }
        private void ModificarCalifV()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblX.Text = dgvCalificaciones.CurrentRow.Cells["idCalif"].Value.ToString();
            cmbGrupo.Text = dgvCalificaciones.CurrentRow.Cells["Grupo"].Value.ToString();
            cmbAlumnos.Text = dgvCalificaciones.CurrentRow.Cells["Alumno"].Value.ToString();
            cmbMateria.Text = dgvCalificaciones.CurrentRow.Cells["Materia"].Value.ToString();
            txtParcial1.Text = dgvCalificaciones.CurrentRow.Cells["Parcial1"].Value.ToString();
            txtParcial2.Text = dgvCalificaciones.CurrentRow.Cells["Parcial2"].Value.ToString();
            txtParcial3.Text = dgvCalificaciones.CurrentRow.Cells["Parcial3"].Value.ToString();
            txtParcial4.Text = dgvCalificaciones.CurrentRow.Cells["Parcial4"].Value.ToString();
        }
        //----------------------------------------------------------------------//
        private void obtenerGrupos(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupos(dxd.ToString());
            cmbGrupo.DisplayMember = "nombreGrupo";
            cmbGrupo.ValueMember = "idGrupo";
        }
        private void obtenerMaterias(string filtro)
        {
            cmbMateria.DataSource = _materiasManejador.getMaterias(dxd.ToString());
            cmbMateria.DisplayMember = "nombreMat";
            cmbMateria.ValueMember = "idMateria";
        }
        private void obtenerAlumnos(string filtro)
        {
            cmbAlumnos.DataSource = _alumnoManejador.GetAlumnos(dxd.ToString());
            cmbAlumnos.DisplayMember = "nombreal";
            cmbAlumnos.ValueMember = "ncontrol";
        }
        //----------------------------------------------------------------------//
        private void Calificaciones_Load(object sender, EventArgs e)
        {
            //BuscarCalificaciones("");
            BuscarVista("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void CmbGrupo_Click(object sender, EventArgs e)
        {
            obtenerGrupos("");
        }

        private void CmbMateria_Click(object sender, EventArgs e)
        {
            obtenerMaterias("");
        }

        private void CmbAlumnos_Click(object sender, EventArgs e)
        {
            obtenerAlumnos("");
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //BuscarCalificaciones("");
            BuscarVista("");
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarCalif();
                    //  BuscarCalificaciones("");
                    BuscarVista("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarCalif();
                LimpiarCuadros();
                lblX.Text = "0";
                //BuscarCalificaciones("");
                BuscarVista("");
                cmbGrupo.Enabled = true;
                cmbMateria.Enabled = true;
                cmbAlumnos.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DgvCalificaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cmbGrupo.Enabled = false;
            cmbMateria.Enabled = false;
            cmbAlumnos.Enabled = false;
            try
            {
                //ModificarCalif();
                // BuscarCalificaciones("");
                ModificarCalifV();
                BuscarVista("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            exportarDataGRidViewExcel(dgvCalificaciones);
        }
        private void exportarDataGRidViewExcel(DataGridView dgv)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el dgv llenando la hoja de trabajo
                    for (int i = 0; i < dgv.Rows.Count; i++)
                    {
                        for (int j = 0; j < dgv.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Falló la creación de archivo, intenta de nuevo con otro nombre", "Error de creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
