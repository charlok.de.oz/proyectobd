﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;

namespace ControlEscolar
{
    public partial class Grupos : Form
    {
        System.Data.DataTable dxd = new System.Data.DataTable();
        private GrupoManejador _grupoManejador;
        public Grupos()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();
        }
        //--------------------------------------------------------------------------//
        private void BuscarGrupos(string filtro)
        {
            dgvGrupos.DataSource = _grupoManejador.GetGrupos(filtro);
        }
        private void EliminarGrupo()
        {
            var idGrupo = dgvGrupos.CurrentRow.Cells["idGrupo"].Value;
            _grupoManejador.EliminarGrupo(Convert.ToInt32(idGrupo));
        }
        private void GuardarGrupo()
        {
            _grupoManejador.GuardarGrupo(new Entidades.ControlEscolar.Grupos
            {
                idGrupo= Convert.ToInt32(lblX.Text),
                NombreGrupo=txtNombre.Text,
                Turno=txtTurno.Text,
            });;
        }
        private void ModificarGrupo()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblX.Text = dgvGrupos.CurrentRow.Cells["idGrupo"].Value.ToString();
            txtNombre.Text = dgvGrupos.CurrentRow.Cells["NombreGrupo"].Value.ToString();
            txtTurno.Text = dgvGrupos.CurrentRow.Cells["Turno"].Value.ToString();
        }
        //-------------------------------------------------------------------------//
        //-------------------------------------------------------------------------//
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtTurno.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtTurno.Text = "";
        }
        //-------------------------------------------------------------------------//

        private void BtnAlumnos_Click(object sender, EventArgs e)
        {
            GrupoAlumnos gpa = new GrupoAlumnos();
            gpa.ShowDialog();
        }

        private void BtnMA_Click(object sender, EventArgs e)
        {
            GrupoMaestrosMaterias gpa2 = new GrupoMaestrosMaterias();
            gpa2.ShowDialog();
        }

        private void BtnCalif_Click(object sender, EventArgs e)
        {
            Calificaciones calif = new Calificaciones();
            calif.ShowDialog();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void Grupos_Load(object sender, EventArgs e)
        {
            BuscarGrupos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarGrupo();
                    BuscarGrupos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupo();
                LimpiarCuadros();
                lblX.Text = "0";
                BuscarGrupos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                ModificarGrupo();
                BuscarGrupos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupos(txtBuscar.Text);
        }
    }
}
