﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class Materias : Form
    {
        DataTable dxd = new DataTable();
        private MateriasManejador _materiasManejador;
        private RelacionManejador _relacionManejador;
        private VistaMateriasManejador _vistaMateriasManejador;
        public Materias()
        {
            InitializeComponent();
            _materiasManejador = new MateriasManejador();
            _relacionManejador = new RelacionManejador();
            _vistaMateriasManejador = new VistaMateriasManejador();
        }
        //---------------------------------------------------
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtCarrera.Enabled = activar;
            txtID.Enabled = activar;
            txtNombre.Enabled = activar;
            txtPractica.Enabled = activar;
            txtSemestre.Enabled = activar;
            txtTeoria.Enabled = activar;
            txtCreditos.Enabled = activar;
            cmbAnterior.Enabled = activar;
            cmbSiguiente.Enabled = activar;
            lblX.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtCarrera.Text = "";
            txtID.Text = "";
            txtPractica.Text = "";
            txtTeoria.Text = "";
            txtSemestre.Text = "";
            cmbAnterior.Text = "";
            cmbSiguiente.Text = "";
            txtCreditos.Text = "";
            lblX.Text = "";
        }
        //---------------------------------------------------
        private void obtenerMatAnterior(string filtro)
        {
            cmbAnterior.DataSource = _materiasManejador.dtMaterias(dxd);
            cmbAnterior.DisplayMember = "nombreMat";
            cmbAnterior.ValueMember = "idMateria";
        }
        private void obtenerMatSiguiente(string filtro)
        {
            cmbSiguiente.DataSource = _materiasManejador.dtMaterias(dxd);
            cmbSiguiente.DisplayMember = "nombreMat";
            cmbSiguiente.ValueMember = "idMateria";
        }
        //---------------------------------------------------
        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMateria(txtBuscar.Text);
           // BuscarRelacion(txtBuscar.Text);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }
        //------------------------------------------
         private void BuscarMateria(string filtro)
        {
            dgvMateria.DataSource = _materiasManejador.getMaterias(filtro);
        }
        private void BuscarRelacion(string filtro)
        {
          //  dgvRelacion.DataSource = _relacionManejador.getRelacion(filtro);
        }
        private void EliminarRelacion()
        {
           
        }
        private void EliminarMateria()
        {
            var nombre = dgvMateria.CurrentRow.Cells["nombreMat"].Value;
            _materiasManejador.EliminarMateria(nombre.ToString());
        }
        private void GuardarMateria()
        {
            _materiasManejador.GuardarMateria(new Entidades.ControlEscolar.Materias
            {
                idMateria= txtID.Text,
                NombreMat = txtNombre.Text,
                fkMatAnt=cmbAnterior.Text,
                fkMatSig=cmbSiguiente.Text,
                Carrera=txtCarrera.Text,
                Semestre=txtSemestre.Text,
                HorasTeo = Convert.ToInt32(txtTeoria.Text),
                HorasPrac = Convert.ToInt32(txtPractica.Text),
                Creditos=(Convert.ToInt32(txtTeoria.Text)+ Convert.ToInt32(txtPractica.Text)),
            }, int.Parse(lblX.Text));
        }
      /*  private void GuardarRelacion()
        {
            _relacionManejador.GuardarRelacion(new Relacion
            {
                idRelacion = int.Parse(lblX.Text),
                Carrera = txtCarrera.Text,
                Semestre=txtSemestre.Text,
                Creditos = Convert.ToInt32(int.Parse(txtTeoria.Text)+int.Parse(txtPractica.Text)),
                fkMatAnt = cmbAnterior.SelectedValue.ToString(),
                fkMatSig=cmbSiguiente.SelectedValue.ToString(),
                fkMatPrinc=txtID.Text,
            }, int.Parse(lblX.Text));
        }*/
        private void ModificarMateria()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            txtID.Text = dgvMateria.CurrentRow.Cells["idMateria"].Value.ToString();
            txtNombre.Text = dgvMateria.CurrentRow.Cells["NombreMat"].Value.ToString();
            txtTeoria.Text = dgvMateria.CurrentRow.Cells["HorasTeo"].Value.ToString();
            txtPractica.Text = dgvMateria.CurrentRow.Cells["HorasPrac"].Value.ToString();
            //txtNombre.Text = dgvMateria.CurrentRow.Cells["Principal"].Value.ToString();
            cmbAnterior.Text = dgvMateria.CurrentRow.Cells["fkMatAnt"].Value.ToString();
            cmbSiguiente.Text = dgvMateria.CurrentRow.Cells["fkMatSig"].Value.ToString();
            txtCarrera.Text = dgvMateria.CurrentRow.Cells["Carrera"].Value.ToString();
            txtSemestre.Text = dgvMateria.CurrentRow.Cells["Semestre"].Value.ToString();
            txtCreditos.Text = dgvMateria.CurrentRow.Cells["Creditos"].Value.ToString();
            
        }
        private void ModificarRelacion()
        {
         /*   ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            txtNombre.Text = dgvRelacion.CurrentRow.Cells["fkMatPrinc"].Value.ToString();
            txtCarrera.Text = dgvRelacion.CurrentRow.Cells["Carrera"].Value.ToString();
            txtCreditos.Text = dgvRelacion.CurrentRow.Cells["Creditos"].Value.ToString();
            cmbAnterior.Text = dgvRelacion.CurrentRow.Cells["fkMatAnt"].Value.ToString();
            cmbSiguiente.Text = dgvRelacion.CurrentRow.Cells["fkMatSig"].Value.ToString();*/
        }

        private void Materias_Load(object sender, EventArgs e)
        {
            BuscarMateria("");
           // BuscarRelacion("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                  //  EliminarRelacion();
                    EliminarMateria();
                    BuscarMateria("");
                   // BuscarRelacion("");
                    LimpiarCuadros();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                //GuardarRelacion();
                GuardarMateria();
                LimpiarCuadros();
                BuscarMateria("");
                //BuscarRelacion("");
                lblX.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void DgvMateria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblX.Text = "1";
            try
            {
                ModificarMateria();
            //    ModificarRelacion();
                BuscarMateria("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DgvRelacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          /*  lblX.Text = "1";
            try
            {
                ModificarRelacion();
                BuscarRelacion("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/
        }

        private void CmbAnterior_Click(object sender, EventArgs e)
        {
            obtenerMatAnterior("");
        }

        private void CmbSiguiente_Click(object sender, EventArgs e)
        {
            obtenerMatSiguiente("");
        }

        private void DgvMateria_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //----------------------------------------
    }
}
