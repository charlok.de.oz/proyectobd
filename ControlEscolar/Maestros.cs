﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class Maestros : Form
    {
        DataTable dxd = new DataTable();
        private MaestroManejador _maestroManejador;
        private MunicipioManejador _municipioManejador;
        private EstadoManejador _estadoManejador;
        public Maestros()
        {
            InitializeComponent();
            _maestroManejador = new MaestroManejador();
            _municipioManejador = new MunicipioManejador();
            _estadoManejador = new EstadoManejador();
            dtpFN.Format = DateTimePickerFormat.Custom;
            dtpIngreso.Format= DateTimePickerFormat.Custom;
            dtpFN.CustomFormat = "yyyy-MM-dd";
            dtpIngreso.CustomFormat = "yyyy";
        }
        /// /////////////////////////////////////////////
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            txtCedula.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtTitulo.Enabled = activar;
            txtIngreso.Enabled = activar;
            dtpFN.Enabled = activar;
            cmbMunicipios.Enabled = activar;
            cmbEstados.Enabled = activar;
            lblX.Enabled = activar;
            dtpIngreso.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            txtCedula.Text = "";
            txtDomicilio.Text = "";
            txtTitulo.Text = "";
            dtpFN.Text = "";
            txtIngreso.Text = "";
            cmbEstados.Text = "";
            cmbMunicipios.Text = "";
            lblX.Text = "0";
            lblX.Text = "";
            cmbid.Text = "";
            dtpIngreso.Text = "";
            
        }
        /////////////////////////////////////////////////////
        private void obtenerestados(string filtro)
        {
            cmbEstados.DataSource = _estadoManejador.dtEstado(dxd);
            cmbEstados.DisplayMember = "nombree";
            cmbEstados.ValueMember = "idest";
        }
        private void obtenermunicipios(string filtro)
        {
            cmbMunicipios.DataSource = _municipioManejador.dtMunicipio(cmbEstados.SelectedValue.ToString());
            cmbMunicipios.DisplayMember = "nombremun";
        }
        /////////////////////////////////////////////////////
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }
        /////////////////////////////////////////////////////
        private void BuscarMaestros(string filtro)
        {
            dgvMaestro.DataSource = _maestroManejador.GetMaestro(filtro);
        }
        private void EliminarMaestro()
        {
            var nombre = dgvMaestro.CurrentRow.Cells["nombreP"].Value;
            _maestroManejador.EliminarMaestro(nombre.ToString());
        }
        private void agarraid(string filtro)
        {

            cmbid.DataSource = _maestroManejador.idMaestro(filtro);
            cmbid.DisplayMember = "NextID";
            cmbid.ValueMember = "NextID";
            if (cmbid.Text == "")
            {
                lblID.Text = "D" + dtpIngreso.Text + "0" + "1";
            }
            else if((int.Parse(cmbid.SelectedValue.ToString())) < 10)
            {
                lblID.Text = "D" + dtpIngreso.Text +"0"+ cmbid.Text;
            }
            else
            {
                lblID.Text = "D" + dtpIngreso.Text + cmbid.Text;
            }
            
        }
        /*private void tomarid()
        {
            int v=Convert.ToInt32(cmbid.Text);
            v = v + 1;
            txtID.Text = v.ToString();
            txtAidi.Text= "D" + txtIngreso.Text + "0" + txtID.Text;
          
        }*/
        private void GuardarMaestro()
        {
            _maestroManejador.GuardarMaestro(new Maestro
            {
                IDProfe = lblID.Text,
                NombreP = txtNombre.Text,
                AppProfe = txtApellidop.Text,
                ApmProfe = txtApellidom.Text,
                DireccionP = txtDomicilio.Text,
                MunicipioP = cmbMunicipios.Text,
                EstadoP = cmbEstados.Text,
                NumCedulaP = Convert.ToInt32(txtCedula.Text),
                TituloP=txtTitulo.Text,
                FechaNacP = dtpFN.Text,
            },int.Parse(lblX.Text));
        }
        private void ModificarMaestro()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblID.Text= dgvMaestro.CurrentRow.Cells["IDProfe"].Value.ToString();
            txtNombre.Text = dgvMaestro.CurrentRow.Cells["NombreP"].Value.ToString();
            txtApellidop.Text = dgvMaestro.CurrentRow.Cells["AppProfe"].Value.ToString();
            txtApellidom.Text = dgvMaestro.CurrentRow.Cells["ApmProfe"].Value.ToString();
            txtDomicilio.Text = dgvMaestro.CurrentRow.Cells["DireccionP"].Value.ToString();
            cmbMunicipios.Text = dgvMaestro.CurrentRow.Cells["MunicipioP"].Value.ToString();
            cmbEstados.Text = dgvMaestro.CurrentRow.Cells["EstadoP"].Value.ToString();
            txtCedula.Text = dgvMaestro.CurrentRow.Cells["NumCedulaP"].Value.ToString();
            txtTitulo.Text = dgvMaestro.CurrentRow.Cells["TituloP"].Value.ToString();
            dtpFN.Text = dgvMaestro.CurrentRow.Cells["FechaNacP"].Value.ToString();
        }

        private void Maestros_Load(object sender, EventArgs e)
        {
            BuscarMaestros("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarMaestro();
                    BuscarMaestros("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            
            try
            {
                GuardarMaestro();
                LimpiarCuadros();
                BuscarMaestros("");
                cmbid.Text="";
                lblX.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void DgvMaestro_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblX.Text = "1";
            try
            {
                ModificarMaestro();
                BuscarMaestros("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaestros(txtBuscar.Text);
        }

        private void CmbEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            obtenermunicipios("");
        }

        private void CmbEstados_Click(object sender, EventArgs e)
        {
            obtenerestados("");
        }

        private void TxtIngreso_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void DgvMaestro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            frmCarreras carrera = new frmCarreras();
            carrera.ShowDialog();
        }

        private void BtnGuardar2_Click(object sender, EventArgs e)
        {
        }

        private void DtpIngreso_ValueChanged(object sender, EventArgs e)
        {
            agarraid(dtpIngreso.Text);

        }
    }
}
