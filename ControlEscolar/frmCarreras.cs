﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class frmCarreras : Form
    {
        DataTable dxd = new DataTable();
        private CarreraManejador _carreraManejador;
        private VistaManejador _vistaManejador;
        private OpenFileDialog _archivoPDF;
        private string _rutaPDF;
        public frmCarreras()
        {
            InitializeComponent();
            _carreraManejador = new CarreraManejador();
            _vistaManejador = new VistaManejador();
            _archivoPDF = new OpenFileDialog();
            _rutaPDF = Application.StartupPath + "\\pdf\\";
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtID.Enabled = activar;
            txtTitulo.Enabled = activar;
            cmbMaestros.Enabled = activar;
            txtDoc.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtBuscar.Text = "";
            txtID.Text = "0";
            txtDoc.Text = "";
            cmbMaestros.Text = "";
            txtTitulo.Text = "";
        }
        private void obtenerMaestros(string filtro)
        {
            cmbMaestros.DataSource = _carreraManejador.dtMaestro(dxd);
            cmbMaestros.ValueMember = "idProfe";
            cmbMaestros.DisplayMember = "nombreP";
            
        }
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            LimpiarCuadros();
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }
        private void BuscarCarrera(string filtro)
        {
            dgvCarreras.DataSource = _vistaManejador.GetVista(filtro);
        }
        private void EliminarCarrera()
        {
            string rt = dgvCarreras.CurrentRow.Cells["Documentos"].Value.ToString();
            var nombre = dgvCarreras.CurrentRow.Cells["ID"].Value;
            _carreraManejador.EliminarCarrera(nombre.ToString());
            EliminarArchivoPDF(_rutaPDF + rt);
        }
        private void GuardarCarrera()
        {

            _carreraManejador.GuardarCarrera(new Carreras
            {
                IDEstudio = int.Parse(txtID.Text),
                Titulo = txtTitulo.Text,
                FKID = cmbMaestros.SelectedValue.ToString(),
                Documentos=txtDoc.Text,
            },int.Parse(lblX.Text));
        }
        private void ModificarCarrera()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            txtID.Text = dgvCarreras.CurrentRow.Cells["ID"].Value.ToString();
            txtTitulo.Text = dgvCarreras.CurrentRow.Cells["Titulo"].Value.ToString();
            cmbMaestros.Text = dgvCarreras.CurrentRow.Cells["IDMaestro"].Value.ToString();
            txtDoc.Text = dgvCarreras.CurrentRow.Cells["Documentos"].Value.ToString();
        }

        private void FrmCarreras_Load(object sender, EventArgs e)
        {
            BuscarCarrera("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarCarrera(txtBuscar.Text);
        }

        private void DgvCarreras_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblX.Text = "1";
            try
            {
                ModificarCarrera();
                BuscarCarrera("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarCarrera();
                GuardarArchivoPDF();
                LimpiarCuadros();
                BuscarCarrera("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    
                    EliminarCarrera();
                    BuscarCarrera("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void CmbMaestros_Click(object sender, EventArgs e)
        {
            obtenerMaestros("");

        }

        private void BtnGuardar2_Click(object sender, EventArgs e)
        {
        }
        private void CargarArchivoPDF()
        {
            _archivoPDF.Filter = "Archivo tipo (*.pdf)|*.pdf";
            _archivoPDF.Title = "Cargar Archivo";
            _archivoPDF.ShowDialog();

            if (_archivoPDF.FileName != "")
            {
                var archivo = new FileInfo(_archivoPDF.FileName);

                txtDoc.Text = archivo.Name;
            }
        }
        private void GuardarArchivoPDF()
        {
            try
            {
                if (_archivoPDF.FileName != null)
                {
                    if (_archivoPDF.FileName != "")
                    {
                        var archivo = new FileInfo(_archivoPDF.FileName);
                        if (Directory.Exists(_rutaPDF))
                        {
                            //Codigo para  ++ el archivo
                            var obtenerArchivos = Directory.GetFiles(_rutaPDF, "*.pdf");
                            FileInfo archivoAnterior;
                            if (obtenerArchivos.Length != 0)
                            {
                                //Codigo Para reemplazar imagen
                                archivoAnterior = new FileInfo(obtenerArchivos[0]);
                                if (archivoAnterior.Exists)
                                {

                                    archivo.CopyTo(_rutaPDF + archivo.Name);
                                }
                            }
                            else
                            {
                                archivo.CopyTo(_rutaPDF + archivo.Name);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(_rutaPDF);
                            archivo.CopyTo(_rutaPDF + archivo.Name);
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Archivo Duplicado");
            }
        
        }
        private void EliminarArchivoPDF(string ruta)
        {
            try
            {
                if (File.Exists(@ruta))
                {
                    File.Delete(@ruta);
                    txtDoc.Clear();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No existe");
            } 
        }

        private void BtnSubir_Click(object sender, EventArgs e)
        {
            CargarArchivoPDF();
        }

        private void DgvCarreras_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
