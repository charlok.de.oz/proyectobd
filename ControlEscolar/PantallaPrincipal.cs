﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuario usuario = new frmUsuario();
            usuario.ShowDialog();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlumno alumno = new frmAlumno();
            alumno.ShowDialog();
        }

        private void CatalogosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MaestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Maestros maestros = new Maestros();
            maestros.ShowDialog();

        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materias materias = new Materias();
            materias.ShowDialog();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Escuela escuela = new Escuela();
            escuela.ShowDialog();
        }

        private void GruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Grupos gpa = new Grupos();
            gpa.ShowDialog();
        }
    }
}
