﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmUsuario : Form
    {
        private UsuarioManejador _usuarioManejador;
        Usuario _usuario;
        public frmUsuario()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
        }
        private void cargarUsuario()
        {
            _usuario.IdUsuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.ApellidoPaterno = txtApellidop.Text;
            _usuario.ApellidoMaterno = txtApellidom.Text;
            _usuario.Contrasenia = txtContrasenia.Text;
        }
        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void BuscarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = _usuarioManejador.GetUsuarios(filtro);
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }

        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            txtContrasenia.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            txtContrasenia.Text = "";
            lblId.Text = "0";
        }
        private void DgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Estas seguro que deseas eliminar este registro","eliminar registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            try
            {
                EliminarUsuario();
                BuscarUsuarios("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarUsuario()
        {
            _usuarioManejador.Guardar(_usuario);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            //ASD
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
           //ASD
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            //ASD
        }
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //ASD
        }
        private void EliminarUsuario()
        {
            var IdUsuario = dgvUsuarios.CurrentRow.Cells["idusuario"].Value;
            _usuarioManejador.eliminar(Convert.ToInt32(IdUsuario));
        }

        private void DgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
                BuscarUsuarios("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }

        private void ModificarUsuario()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblId.Text = dgvUsuarios.CurrentRow.Cells["IdUsuario"].Value.ToString();
            txtNombre.Text = dgvUsuarios.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidop.Text = dgvUsuarios.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            txtApellidom.Text = dgvUsuarios.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
            txtContrasenia.Text = dgvUsuarios.CurrentRow.Cells["Contrasenia"].Value.ToString();
        }

        private void BtnNuevo_Click_1(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }
        private bool validarUsuario()
        {
            var tupla = _usuarioManejador.ValidarUsuarii(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido)
            {
                MessageBox.Show(mensaje,"Error de validacion");
            }
            return valido;
        }
        private void BtnGuardar_Click_1(object sender, EventArgs e)
        {
            cargarUsuario();

            if (validarUsuario())
            {
                ControlarBotones(true, false, false, true);
                ControlarCuadros(false);
                try
                {
                    GuardarUsuario();
                    LimpiarCuadros();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged_1(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }
    }
}
