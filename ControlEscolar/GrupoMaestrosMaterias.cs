﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class GrupoMaestrosMaterias : Form
    {
        System.Data.DataTable dxd = new System.Data.DataTable();
        private GrupoManejador _grupoManejador;
        private MateriasManejador _materiasManejador;
        private MaestroManejador _maestroManejador;
        private GruposMAManejador _gruposMAManejador;
        public GrupoMaestrosMaterias()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();
            _materiasManejador = new MateriasManejador();
            _maestroManejador = new MaestroManejador();
            _gruposMAManejador = new GruposMAManejador();
        }
        //----------------------------------------------------------------------//
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbMateria.Enabled = activar;
            cmbMaestro.Enabled = activar;
            cmbGrupo.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            cmbGrupo.Text = "";
            cmbMaestro.Text = "";
            cmbMateria.Text = "";
        }
        //----------------------------------------------------------------------//
        private void BuscarGruposMA(string filtro)
        {
            dgvGruposMA.DataSource = _gruposMAManejador.GetGruposMA(filtro);
        }
        private void EliminarGrupoMA()
        {
            var idGrupo = dgvGruposMA.CurrentRow.Cells["idGrupoMatMa"].Value;
            _gruposMAManejador.EliminarGrupoMA(Convert.ToInt32(idGrupo));
        }
        private void GuardarGrupoMA()
        {
            _gruposMAManejador.GuardarGrupoMA(new GruposMA
            {
                idGrupoMatMa = Convert.ToInt32(lblX.Text),
                fkGrupoMA = int.Parse(cmbGrupo.SelectedValue.ToString()),
                fkMaestroMA = cmbMaestro.SelectedValue.ToString(),
                fkMateriaMA = cmbMateria.SelectedValue.ToString(),
                
            }); ;
        }
        private void ModificarGrupoMA()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblX.Text = dgvGruposMA.CurrentRow.Cells["idGrupoMatMa"].Value.ToString();
            cmbGrupo.Text = dgvGruposMA.CurrentRow.Cells["fkGrupoMA"].Value.ToString();
            cmbMaestro.Text = dgvGruposMA.CurrentRow.Cells["fkMaestroMA"].Value.ToString();
            cmbMateria.Text = dgvGruposMA.CurrentRow.Cells["fkMateriaMA"].Value.ToString();
        }
        //----------------------------------------------------------------------//
        private void obtenerGrupos(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupos(dxd.ToString());
            cmbGrupo.DisplayMember = "nombreGrupo";
            cmbGrupo.ValueMember = "idGrupo";
           
            
        }
        private void obtenerMaestros(string filtro)
        {
            cmbMaestro.DataSource = _maestroManejador.GetMaestro(dxd.ToString());
            cmbMaestro.DisplayMember = "nombreP";
            cmbMaestro.ValueMember = "idProfe";

        }
        private  void obtenerMateria(string filtro)
        {
            cmbMateria.DataSource = _materiasManejador.getMaterias(dxd.ToString());
            cmbMateria.DisplayMember = "nombreMat";
            cmbMateria.ValueMember = "idMateria";
           
            
        }
        //----------------------------------------------------------------------//
        private void DgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cmbGrupo.Enabled = false;
            cmbMateria.Enabled = false;
            try
            {
                ModificarGrupoMA();
                BuscarGruposMA("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGruposMA(txtBuscar.Text);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            lblX.Text = "0";
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void GrupoMaestrosMaterias_Load(object sender, EventArgs e)
        {
            BuscarGruposMA("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                try
                {
                    EliminarGrupoMA();
                    BuscarGruposMA("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void CmbGrupo_Click(object sender, EventArgs e)
        {
            obtenerGrupos("");
        }

        private void CmbMateria_Click(object sender, EventArgs e)
        {
            obtenerMateria("");
        }

        private void CmbMaestro_Click(object sender, EventArgs e)
        {
            obtenerMaestros("");
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupoMA();
                LimpiarCuadros();
                lblX.Text = "0";
                BuscarGruposMA("");
                cmbGrupo.Enabled = true;
                cmbMateria.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
