﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class GruposAlumnos
    {
        private int _idGA;
        private int _fk_gp;
        private int _fk_al;
        public int idGruposAlumnos { get => _idGA; set => _idGA = value; }
        public int fkGrupo { get => _fk_gp; set => _fk_gp = value; }
        public int fkAlumno { get => _fk_al; set => _fk_al = value; }
    }
}
