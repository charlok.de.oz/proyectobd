﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class GruposMA
    {
        private int _idGMA;
        private int _fkGP;
        private string _fkMat;
        private string _fkMae;

        public int idGrupoMatMa { get => _idGMA; set => _idGMA = value; }
        public int fkGrupoMA { get => _fkGP; set => _fkGP = value; }
        public string fkMateriaMA { get => _fkMat; set => _fkMat = value; }
        public string fkMaestroMA { get => _fkMae; set => _fkMae = value; }

    }
}
