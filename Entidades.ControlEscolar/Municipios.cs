﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Municipios
    {
        private int _idMuni;
        private string _nombreM;
        private int _fkEsta;
        public int idMuni { get => _idMuni; set => _idMuni = value; }
        public string nombreM { get => _nombreM; set => _nombreM = value; }
        public int fkEsa { get => _fkEsta; set => _fkEsta = value; }
    }
}
