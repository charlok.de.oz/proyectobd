﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Materias
    {
        private string _idMateria;
        private string _nombreMat;
        private string _fkMatA;
        private string _fkMatS;
        private string _carrera;
        private string _semestre;
        private int _hrsT;
        private int _hrsP;
        private int _creditos;
        public string idMateria { get => _idMateria; set => _idMateria = value; }
        public string NombreMat { get => _nombreMat; set => _nombreMat = value; }
        public string fkMatAnt { get => _fkMatA; set => _fkMatA = value; }
        public string fkMatSig { get => _fkMatS; set => _fkMatS = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }
        public int HorasTeo { get => _hrsT; set => _hrsT = value; }
        public int HorasPrac { get => _hrsP; set => _hrsP = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
    }
}
