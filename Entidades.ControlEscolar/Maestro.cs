﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Maestro
    {
        private string _idProfe;
        private string _nombreP;
        private string _appProfe;
        private string _apmProfe;
        private string _direccProfe;
        private string _municP;
        private string _estP;
        private int _numCed;
        private string _titulo;
        private string _fechaNacimientoP;
        public string IDProfe { get => _idProfe; set => _idProfe = value; }
        public string NombreP { get => _nombreP; set => _nombreP = value; }
        public string AppProfe { get => _appProfe; set => _appProfe = value; }
        public string ApmProfe { get => _apmProfe; set => _apmProfe = value; }
        public string DireccionP { get => _direccProfe; set => _direccProfe = value; }
        public string MunicipioP { get => _municP; set => _municP = value; }
        public string EstadoP { get => _estP; set => _estP = value; }
        public int NumCedulaP { get => _numCed; set => _numCed = value; }
        public string TituloP { get => _titulo; set => _titulo = value; }
        public string FechaNacP { get => _fechaNacimientoP; set => _fechaNacimientoP = value; }
    }
}
