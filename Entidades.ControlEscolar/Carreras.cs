﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Carreras
    {
        private int _idEst;
        private string _Titulo;
        private string _fkID;
        private string _documentos;
        public int IDEstudio { get => _idEst; set => _idEst = value; }
        public string Titulo { get => _Titulo; set => _Titulo = value; }
        public string FKID { get => _fkID; set => _fkID = value; }
        public string Documentos { get => _documentos; set => _documentos = value; }
    }
}
