﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Alumno
    {
        private int _ncontrol;
        private string _nombreal;
        private string _apellidoPaternoal;
        private string _apellidoMaternoal;
        private string _fechanacimientoal;
        private string _domicilio;
        private string _correoal;
        private string _sexo;
        private string _Estado;
        private string _Municipio;

        public int NControl { get => _ncontrol; set => _ncontrol = value; }
        public string NombreAl { get => _nombreal; set => _nombreal = value; }
        public string ApellidoPaternoAl { get => _apellidoPaternoal; set => _apellidoPaternoal = value; }
        public string ApellidoMaternoAl { get => _apellidoMaternoal; set => _apellidoMaternoal = value; }
        public string FechaNacimientoAl { get => _fechanacimientoal; set => _fechanacimientoal = value; }
        public string DomicilioAl { get => _domicilio; set => _domicilio = value; }
        public string CorreoAl { get => _correoal; set => _correoal = value; }
        public string SexoAl { get => _sexo; set => _sexo = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }

    }
}
