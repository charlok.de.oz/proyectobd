﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Grupos
    {
        private int _idGrupo;
        private string _nombreG;
        private string _turno;
        public int idGrupo { get => _idGrupo; set => _idGrupo = value; }
        public string NombreGrupo { get => _nombreG; set => _nombreG = value; }
        public string Turno { get => _turno; set => _turno = value; }
    }
}
