﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Calificacion
    {
        private int _idCalificacion;
        private int _fkGrupoC;
        private string _fkMateriaC;
        private int _fkAlumnoC;
        private int _calif1;
        private int _calif2;
        private int _calif3;
        private int _calif4;
        public int idCalificacion { get => _idCalificacion; set => _idCalificacion = value; }
        public int fkGrupoC { get => _fkGrupoC; set => _fkGrupoC = value; }
        public string fkMateriaC { get => _fkMateriaC; set => _fkMateriaC = value; }
        public int fkAlumnoC { get => _fkAlumnoC; set => _fkAlumnoC = value; }
        public int parcial1 { get => _calif1; set => _calif1 = value; }
        public int parcial2 { get => _calif2; set => _calif2 = value; }
        public int parcial3 { get => _calif3; set => _calif3 = value; }
        public int parcial4 { get => _calif4; set => _calif4 = value; }


    }
}
