﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class VistaMaterias
    {
        private string _idMatPrincipal;
        private string _nombreMat;
        private string _fkAnterior;
        private string _fkPrincipal;
        private string _fkSiguiente;
        private int _hrsT;
        private int _hrsP;
        private string _carr;
        private string _sem;
        private int _credi;
        public string IDPrinc { get => _idMatPrincipal; set => _idMatPrincipal = value; }
        public string NombreMat { get => _nombreMat; set => _nombreMat = value; }
        public string Anterior { get => _fkAnterior; set => _fkAnterior = value; }
        public string Principal { get => _fkPrincipal; set => _fkPrincipal = value; }
        public string Siguiente { get => _fkSiguiente; set => _fkSiguiente = value; }
        public int Teoria { get => _hrsT; set => _hrsT = value; }
        public int Practica { get => _hrsP; set => _hrsP = value; }
        public string Carrera { get => _carr; set => _carr = value; }
        public string Semestre { get => _sem; set => _sem = value; }
        public int Creditos { get => _credi; set => _credi = value; }
    }
}
