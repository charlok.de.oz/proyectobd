﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Escuela
    {
        private int _idEsc;
        private string _nombreEsc;
        private string _rfcEsc;
        private string _domicilioEsc;
        private int _telefonoEsc;
        private string _correoEsc;
        private string _pagina;
        private string _nombreEncargado;
        private string _logoEsc;

        public int IDEscuela { get => _idEsc; set => _idEsc = value; }
        public string NombreEsc { get => _nombreEsc; set => _nombreEsc = value; }
        public string RFCEsc { get => _rfcEsc; set => _rfcEsc = value; }
        public string DomicilioEsc { get => _domicilioEsc; set => _domicilioEsc = value; }
        public int TelefonoEsc { get => _telefonoEsc; set => _telefonoEsc = value; }
        public string CorreoEsc { get => _correoEsc; set => _correoEsc = value; }
        public string PaginaEsc { get => _pagina; set => _pagina = value; }
        public string EncargadoEsc { get => _nombreEncargado; set => _nombreEncargado = value; }
        public string LogoEsc { get => _logoEsc; set => _logoEsc = value; }


    }
}
