﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Vista
    {
        private int _id;
        private string _idMaestro;
        private string _nombre;
        private string _apellidoP;
        private string _apellidoM;
        private int _numCedula;
        private string _tituloP;
        private string _titulo;
        private string _documentos;
        public int ID { get => _id; set => _id = value; }
        public string IDMaestro { get => _idMaestro; set => _idMaestro = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellidoP; set => _apellidoP = value; }
        public string ApellidoMaterno { get => _apellidoM; set => _apellidoM = value; }
        public int Cedula { get => _numCedula; set => _numCedula = value; }
        public string TituloPrincipal { get => _tituloP; set => _tituloP = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Documentos { get => _documentos; set => _documentos = value; }

    }
}
