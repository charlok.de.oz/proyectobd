﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Estados
    {
        private int _idEstados;
        private string _nombreEst;
        private string _abrev;
        public int idEstados { get => _idEstados; set => _idEstados = value; }
        public string nombreEst { get => _nombreEst; set => _nombreEst = value; }
        public string abrev { get => _abrev; set => _abrev = value; }
    }
}
