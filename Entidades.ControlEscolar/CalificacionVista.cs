﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class CalificacionVista
    {
        private int _IdCalificacion;
        private string _Grupo;
        private string _Materia;
        private string _Alumno;
        private int _PrimerParcial;
        private int _SegundoParcial;
        private int _TercerParcial;
        private int _CuartoParcial;
        public int IDCalif { get => _IdCalificacion; set => _IdCalificacion = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
        public string Alumno { get => _Alumno; set => _Alumno = value; }
        public int Parcial1 { get => _PrimerParcial; set => _PrimerParcial = value; }
        public int Parcial2 { get => _SegundoParcial; set => _SegundoParcial = value; }
        public int Parcial3 { get => _TercerParcial; set => _TercerParcial = value; }
        public int Parcial4 { get => _CuartoParcial; set => _CuartoParcial = value; }

    }
}
