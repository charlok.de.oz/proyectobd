﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Relacion
    {
        private int _idRelacion;
        private string _carrera;
        private string _semestre;
        private string _fkMatP;
        private string _fkMatA;
        private string _fkMatS;
        private int _creditos;
        public int idRelacion { get => _idRelacion; set => _idRelacion = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
        public string fkMatPrinc { get => _fkMatP; set => _fkMatP = value; }
        public string fkMatAnt { get => _fkMatA; set => _fkMatA = value; }
        public string fkMatSig { get => _fkMatS; set => _fkMatS = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }

    }
}
