﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MunicipioAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public MunicipioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        /*public void consultarMunicipios(string filtro)
        {
            var ds = new DataSet();
            string consulta = "Select nombremun from municipios where fkest='" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "municipios");
        }*/
        public DataTable dtMunicipios(string filtro)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "Select nombremun from municipios where fkest='" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "municipios");

            dt = ds.Tables[0];

            return dt;
        }
        public List<Municipios> GetMunicipios(string filtro)
        {
            var listMunicipios = new List<Municipios>();
            var ds = new DataSet();
            string consulta = "Select nombremun from municipios where fkest='" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "municipios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var municipios = new Municipios
                {
                    nombreM= row["nombremun"].ToString(),
                };
                listMunicipios.Add(municipios);
            }
            return listMunicipios;
        }
    }
}
