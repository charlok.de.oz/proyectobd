﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MaestroAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public MaestroAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Maestro maestro, int x)
        {
            if (x==0)
            {
                string consulta = string.Format("Insert into maestros values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}') ", maestro.IDProfe, maestro.NombreP, maestro.AppProfe, maestro.ApmProfe, maestro.DireccionP, maestro.MunicipioP, maestro.EstadoP, maestro.NumCedulaP, maestro.TituloP, maestro.FechaNacP);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update maestros set  nombreP = '"+maestro.NombreP+"',appp = '"+maestro.AppProfe+"',apmp = '"+maestro.ApmProfe+"',direccion = '"+maestro.DireccionP+"',municipioP = '"+maestro.MunicipioP+"',estadoP = '"+maestro.EstadoP+"',NumCedula = '"+maestro.NumCedulaP+"', titulo='"+maestro.TituloP+"',fechaNacimientoprof='"+maestro.FechaNacP+"' where idProfe = '"+maestro.IDProfe+"'");
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Actualizar(Maestro maestro)
        {
        
        }
        public void eliminar(string Nombre)
        {
            string consulta = string.Format("Delete from maestros where nombreP like '%" + Nombre + "%'");
            conexion.EjecutarConsulta(consulta);
        }
        public DataTable idMaestro(string Fecha)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select max(substring(idProfe,-1)+1)as NextID from maestros where idProfe like '%" + Fecha + "%'";
            ds = conexion.ObtenerDatos(consulta, "maestros");
            dt = ds.Tables[0];
            return dt;
        }
        public int idMaestro2(string Fecha)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select max(substring(idProfe,-2))as NextID from maestros where idProfe like '%" + Fecha + "%'";
            ds = conexion.ObtenerDatos(consulta, "maestros");
            dt = ds.Tables[0];
            int xd;
            xd = int.Parse((dt.ToString())+1);
            return xd;
        }
        public List<Maestro> GetMaestros(string filtro)
        {
            var listMaestro = new List<Maestro>();
            var ds = new DataSet();
            string consulta = "Select * from maestros where nombreP like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "maestros");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var maestros = new Maestro
                {
                    IDProfe = row["idProfe"].ToString(),
                    NombreP = row["nombreP"].ToString(),
                    AppProfe = row["appp"].ToString(),
                    ApmProfe = row["apmp"].ToString(),
                    DireccionP = row["direccion"].ToString(),
                    MunicipioP = row["municipioP"].ToString(),
                    EstadoP = row["estadoP"].ToString(),
                    NumCedulaP = Convert.ToInt32(row["NumCedula"]),
                    TituloP = row["titulo"].ToString(),
                    FechaNacP = row["fechaNacimientoProf"].ToString(),
                };
                listMaestro.Add(maestros);

            }
            return listMaestro;
        }

    }
}
