﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class RelacionAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public RelacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Relacion relacion, int x)
        {
            if (x == 0)
            {
                string consulta = string.Format("Insert into relacion values(null,'{0}','{1}','{2}','{3}','{4}','{5}')",relacion.Carrera,relacion.Semestre,relacion.fkMatPrinc,relacion.fkMatAnt,relacion.fkMatSig,relacion.Creditos);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update relacion set carrera = '{0}',semestre = '{1}',fk_matAnterior = '{2}',fk_matSiguiente = '{3}',creditos = '{4}' where fk_matPrincipal = '{5}'", relacion.Carrera, relacion.Semestre, relacion.fkMatAnt, relacion.fkMatSig, relacion.Creditos, relacion.fkMatPrinc);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(string filtro)
        {
            string consulta = string.Format("Delete from relacion where fk_matPrincipal like '%" + filtro + "%'");
            conexion.EjecutarConsulta(consulta);
        }
        public List<Relacion> getRelacion(string filtro)
        {
            var listRelacion = new List<Relacion>();
            var ds = new DataSet();
            string consulta = "Select * from relacion where fk_matPrincipal like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "relacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var relacion = new Relacion
                {
                    idRelacion = Convert.ToInt32(row["idRelacion"]),
                    Carrera = row["carrera"].ToString(),
                    Semestre = row["semestre"].ToString(),
                    fkMatPrinc = row["fk_matPrincipal"].ToString(),
                    fkMatAnt = row["fk_matAnterior"].ToString(),
                    fkMatSig = row["fk_matSiguiente"].ToString(),
                    Creditos = Convert.ToInt32(row["creditos"])
                };
                listRelacion.Add(relacion);

            }
            return listRelacion;
        }
    }
}
