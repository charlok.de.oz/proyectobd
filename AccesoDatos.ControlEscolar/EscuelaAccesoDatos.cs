﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Escuela escuela, int x)
        {
            if (x == 0)
            {
                string consulta = string.Format("Insert into escuela values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}') ", escuela.IDEscuela,escuela.NombreEsc,escuela.RFCEsc,escuela.DomicilioEsc,escuela.TelefonoEsc,escuela.CorreoEsc,escuela.PaginaEsc,escuela.EncargadoEsc,escuela.LogoEsc);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update escuela set  nombreEsc = '" + escuela.NombreEsc + "',rfcEsc = '" + escuela.RFCEsc + "',domicilioEsc = '" + escuela.DomicilioEsc + "',telefonoEsc = '" + escuela.TelefonoEsc + "',correoEsc = '" + escuela.CorreoEsc + "',pagina = '" + escuela.PaginaEsc + "',nombreEncargado = '" + escuela.EncargadoEsc + "', LogoEsc='" + escuela.LogoEsc + "' where idEsc = '" + escuela.IDEscuela + "'");
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(string Nombre)
        {
            string consulta = string.Format("Delete from escuela where nombreEsc like '%" + Nombre + "%'");
            conexion.EjecutarConsulta(consulta);
        }
        public List<Escuela> GetEscuelas(string filtro)
        {
            var listEscuela = new List<Escuela>();
            var ds = new DataSet();
            string consulta = "Select * from escuela where nombreEsc like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    IDEscuela = Convert.ToInt32(row["idEsc"]),
                    NombreEsc = row["nombreEsc"].ToString(),
                    RFCEsc = row["rfcEsc"].ToString(),
                    DomicilioEsc = row["domicilioEsc"].ToString(),
                    TelefonoEsc = Convert.ToInt32(row["telefonoEsc"]),
                    CorreoEsc = row["correoEsc"].ToString(),
                    PaginaEsc = row["pagina"].ToString(),
                    EncargadoEsc = row["nombreEncargado"].ToString(),
                    LogoEsc = row["LogoEsc"].ToString(),
                    };
                listEscuela.Add(escuela);
            }
            return listEscuela;
        }
        public DataTable limite()
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select count (*) as NextID from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            dt = ds.Tables[0];
            return dt;
        }
        public DataSet consulta()
        {
            DataSet ds;
            string consulta = "Select * from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;
        }
    }
}
