﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class VistaAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public VistaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<Vista> GetVista(string filtro)
        {
            var listVista = new List<Vista>();
            var ds = new DataSet();
            string consulta = "Select * from v_carreras where Nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_carreras");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var vista = new Vista
                {
                    ID = Convert.ToInt32(row["ID"]),
                    IDMaestro = row["IDMaestro"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    Cedula = Convert.ToInt32(row["Cedula"].ToString()),
                    TituloPrincipal = row["TituloPrincipal"].ToString(),
                    Titulo = row["Titulo"].ToString(),
                    Documentos = row["Documentos"].ToString(),
                };
                listVista.Add(vista);

            }
            return listVista;
        }
    }
}
