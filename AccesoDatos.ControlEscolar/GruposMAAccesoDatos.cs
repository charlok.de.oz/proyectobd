﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class GruposMAAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public GruposMAAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(GruposMA gruposMA)
        {
            if (gruposMA.idGrupoMatMa == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into GruposMP values(null,'{0}','{1}','{2}')", gruposMA.fkGrupoMA,gruposMA.fkMaestroMA,gruposMA.fkMateriaMA);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update GruposMP set fk_profes = '{0}' where idMP = {1}", gruposMA.fkMaestroMA, gruposMA.idGrupoMatMa);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int idGrupo)
        {
            //Eliminar
            string consulta = string.Format("Delete from GruposMP where idMP = '{0}'", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<GruposMA> GetGruposMA(string filtro)
        {
            var listGruposMA = new List<GruposMA>();
            var ds = new DataSet();
            string consulta = "Select * from GruposMP where fk_profes like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "GruposMP");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var gruposMP = new GruposMA
                {

                    idGrupoMatMa = Convert.ToInt32(row["idMP"]),
                    fkGrupoMA = Convert.ToInt32(row["fk_gp"]),
                    fkMaestroMA= row["fk_profes"].ToString(),
                    fkMateriaMA = row["fk_materia"].ToString(),
                };
                listGruposMA.Add(gruposMP);

            }
            return listGruposMA;
        }
    }
}
