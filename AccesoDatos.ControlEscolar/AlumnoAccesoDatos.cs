﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Alumno alumno)
        {
            if (alumno.NControl == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into alumnos values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}') ", alumno.NombreAl,alumno.ApellidoPaternoAl, alumno.ApellidoMaternoAl, alumno.FechaNacimientoAl, alumno.DomicilioAl, alumno.CorreoAl, alumno.SexoAl,alumno.Estado,alumno.Municipio);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update alumnos set nombreal = '{0}',apellidopaternoal = '{1}',apellidomaternoal = '{2}',fechanacimientoal = '{3}',domicilio = '{4}',correoal = '{5}',sexo = '{6}', fkesta='{7}',fkmuni='{8}' where ncontrol = {9}", alumno.NombreAl, alumno.ApellidoPaternoAl, alumno.ApellidoMaternoAl, alumno.FechaNacimientoAl, alumno.DomicilioAl, alumno.CorreoAl, alumno.SexoAl, alumno.Estado, alumno.Municipio, alumno.NControl);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int nControl)
        {
            //Eliminar
            string consulta = string.Format("Delete from alumnos where ncontrol = '{0}'", nControl);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Alumno> GetAlumnos(string filtro)
        {
            var listAlumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta= "Select * from alumnos where nombreal like '%" + filtro + "%'";
         
            ds = conexion.ObtenerDatos(consulta, "alumnos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                
                var alumnos = new Alumno
                {
                    NControl = Convert.ToInt32(row["ncontrol"]),
                    NombreAl = row["nombreal"].ToString(),
                    ApellidoPaternoAl = row["apellidopaternoal"].ToString(),
                    ApellidoMaternoAl = row["apellidomaternoal"].ToString(),
                    FechaNacimientoAl = row["fechanacimientoal"].ToString(),
                    DomicilioAl = row["domicilio"].ToString(),
                    CorreoAl = row["correoal"].ToString(),
                    SexoAl = row["sexo"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString()
                };
                listAlumno.Add(alumnos);

            }
            return listAlumno;
        }
    }
}
