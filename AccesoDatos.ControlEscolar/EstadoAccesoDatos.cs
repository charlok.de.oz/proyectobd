﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstadoAccesoDatos
    {
        
        ConexionAccesoDatos conexion;
        public EstadoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public DataTable dtEstado(DataTable dt)
        {
            var ds = new DataSet();
            string consulta = "Select nombree, idest from estados";
            ds = conexion.ObtenerDatos(consulta, "estados");

            dt = ds.Tables[0];

            return dt;
        }
        public DataTable idEstado(string filtro)
        {
            var ds = new DataSet();
            var dt = new DataTable();
            string consulta = "Select idest from estados where nombree like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "estados");

            dt = ds.Tables[0];

            return dt;
        }
        public string dtEstadoID(string filtro)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "Select idest from estados where estados.nombree='" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "estados");

            dt = ds.Tables[0];

            return dt.ToString();
        }
        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "Select nombree,idest from estados";
            ds = conexion.ObtenerDatos(consulta, "estados");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var estado = new Estados
                {
                    nombreEst= row["nombree"].ToString(),
                };
                listEstados.Add(estado);

            }
            return listEstados;
        }
    }
}
