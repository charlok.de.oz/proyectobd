﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class VistaMateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public VistaMateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<VistaMaterias> GetVistaMaterias(string filtro)
        {
            var listVistaMaterias = new List<VistaMaterias>();
            var ds = new DataSet();
            string consulta = "Select * from v_materias where NombreMateria like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_materias");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var vista = new VistaMaterias
                {
                    IDPrinc = row["IDMateria"].ToString(),
                    NombreMat = row["NombreMateria"].ToString(),
                    Anterior = row["MatAnterior"].ToString(),
                    Principal = row["MatPrincipal"].ToString(),
                    Siguiente = row["MatSiguiente"].ToString(),
                    Teoria = Convert.ToInt32(row["HorasTeoria"].ToString()),
                    Practica = Convert.ToInt32(row["HorasPractica"].ToString()),
                    Carrera = row["Carrera"].ToString(),
                    Semestre = row["Semestre"].ToString(),
                    Creditos = Convert.ToInt32(row["Creditos"].ToString()),
                };
                listVistaMaterias.Add(vista);

            }
            return listVistaMaterias;
        }
    }
}
