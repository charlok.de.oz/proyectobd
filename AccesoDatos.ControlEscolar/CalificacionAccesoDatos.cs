﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class CalificacionAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CalificacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Calificacion calificacion)
        {
            if (calificacion.idCalificacion == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into Calificacion values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", calificacion.fkAlumnoC,calificacion.fkGrupoC,calificacion.fkMateriaC,calificacion.parcial1,calificacion.parcial2,calificacion.parcial3,calificacion.parcial4);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update Calificacion set parcial1 = '{0}', parcial2 = '{1}',parcial3 = '{2}',parcial4 = '{3}' where idCalif = {4}", calificacion.parcial1, calificacion.parcial2, calificacion.parcial3, calificacion.parcial4,calificacion.idCalificacion);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int idCalificacion)
        {
            //Eliminar
            string consulta = string.Format("Delete from Calificacion where idCalif = '{0}'", idCalificacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Calificacion> GetCalificacion(string filtro)
        {
            var listCalificacion = new List<Calificacion>();
            var ds = new DataSet();
            string consulta = "Select * from Calificacion where fk_alumno like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "Calificacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var Calificacion = new Calificacion
                {

                    idCalificacion = Convert.ToInt32(row["idCalif"]),
                    fkAlumnoC = Convert.ToInt32(row["fk_alumno"]),
                    fkGrupoC = Convert.ToInt32(row["fk_grp"]),
                    fkMateriaC = row["fk_mat"].ToString(),
                    parcial1 = Convert.ToInt32(row["parcial1"]),
                    parcial2 = Convert.ToInt32(row["parcial2"]),
                    parcial3 = Convert.ToInt32(row["parcial3"]),
                    parcial4 = Convert.ToInt32(row["parcial4"]),
                };
                listCalificacion.Add(Calificacion);

            }
            return listCalificacion;
        }
    }
}
