﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;
namespace AccesoDatos.ControlEscolar
{
    public class CalificacionVistaAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CalificacionVistaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<CalificacionVista> GetCalificacionVistas(string filtro)
        {
            var listCalificacionVista = new List<CalificacionVista>();
            var ds = new DataSet();
            string consulta = "Select * from v_calificacion where Alumno like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_calificacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var vista = new CalificacionVista
                {
                    IDCalif = Convert.ToInt32(row["IdCalificacion"].ToString()),
                    Grupo = row["Grupo"].ToString(),
                    Materia = row["Materia"].ToString(),
                    Alumno = row["Alumno"].ToString(),
                    Parcial1 = Convert.ToInt32(row["PrimerParcial"].ToString()),
                    Parcial2 = Convert.ToInt32(row["SegundoParcial"].ToString()),
                    Parcial3 = Convert.ToInt32(row["TercerParcial"].ToString()),
                    Parcial4 = Convert.ToInt32(row["CuartoParcial"].ToString()),
                };
                listCalificacionVista.Add(vista);

            }
            return listCalificacionVista;
        }
    }
}
