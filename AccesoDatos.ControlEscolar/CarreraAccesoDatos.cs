﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CarreraAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CarreraAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Carreras carreras, int x)
        {
            if (x == 0)
            {
                string consulta = string.Format("Insert into estudios values(null,'{0}','{1}','{2}') ", carreras.Titulo,carreras.Documentos, carreras.FKID);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update estudios set Titulo = '"+carreras.Titulo+"', Documento='"+carreras.Documentos+ "', fkprofe= '" + carreras.FKID + "' where idEst='" + carreras.IDEstudio+"'");
                conexion.EjecutarConsulta(consulta);
            } 
        }
        public void eliminar(string filtro)
        {
            string consulta = string.Format("Delete from estudios where idEst like '%" + filtro + "%'");
            conexion.EjecutarConsulta(consulta);
        }
        public List<Carreras> GetCarreras(string filtro)
        {
            var listCarreras = new List<Carreras>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where idEst like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var carreras = new Carreras
                {
                    IDEstudio = Convert.ToInt32(row["idEst"]),
                    Titulo = row["Titulo"].ToString(),
                    Documentos = row["Documento"].ToString(),
                    FKID = row["fkprofe"].ToString(),
                };
                listCarreras.Add(carreras);

            }
            return listCarreras;
        }
        public DataTable dtMaestros(DataTable dt)
        {
            var ds = new DataSet();
            string consulta = "Select nombreP, idProfe from maestros";
            ds = conexion.ObtenerDatos(consulta, "maestros");

            dt = ds.Tables[0];

            return dt;
        }
    }
}
