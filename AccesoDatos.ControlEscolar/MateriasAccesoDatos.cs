﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public MateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Materias materias, int x)
        {
            if (x == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into materias values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", materias.idMateria,materias.NombreMat,materias.fkMatAnt,materias.fkMatSig,materias.Carrera,materias.Semestre,materias.HorasTeo,materias.HorasPrac,materias.Creditos);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update materias set nombreMat = '{0}',fk_matAnterior='{1}',fk_matSiguiente='{2}',carrera='{3}',semestre='{4}',horasT = '{5}',horasP = '{6}',creditos='{7}' where idMateria = {8}", materias.NombreMat, materias.fkMatAnt, materias.fkMatSig, materias.Carrera, materias.Semestre, materias.HorasTeo,materias.HorasPrac, materias.Creditos,materias.idMateria);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(string id)
        {
            string consulta = string.Format("Delete from materias where idMateria like '%" + id + "%'");
            conexion.EjecutarConsulta(consulta);
        }
        public DataTable dtMaterias(DataTable dt)
        {
            var ds = new DataSet();
            string consulta = "Select nombreMat, idMateria from materias";
            ds = conexion.ObtenerDatos(consulta, "materias");

            dt = ds.Tables[0];

            return dt;
        }
        public List<Materias> getMaterias(string filtro)
        {
            var listMaterias = new List<Materias>();
            var ds = new DataSet();
            string consulta = "Select * from materias where idMateria like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "materias");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var materias = new Materias
                {
                    idMateria = row["idMateria"].ToString(),
                    NombreMat = row["nombreMat"].ToString(),
                    fkMatAnt= row["fk_matAnterior"].ToString(),
                    fkMatSig=row["fk_matSiguiente"].ToString(),
                    Carrera= row["carrera"].ToString(),
                    Semestre= row["semestre"].ToString(),
                    HorasTeo = Convert.ToInt32(row["horasT"]),
                    HorasPrac = Convert.ToInt32(row["horasP"]),
                    Creditos= Convert.ToInt32(row["creditos"])
                };
                listMaterias.Add(materias);

            }
            return listMaterias;
        }
    }
}
