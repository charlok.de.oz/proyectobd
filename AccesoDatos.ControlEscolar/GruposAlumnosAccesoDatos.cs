﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class GruposAlumnosAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public GruposAlumnosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(GruposAlumnos gruposAlumnos)
        {
            if (gruposAlumnos.idGruposAlumnos == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into GruposAl values(null,'{0}','{1}')", gruposAlumnos.fkGrupo,gruposAlumnos.fkAlumno);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update GruposAl set fk_grupo = '{0}' where idGA = {1}", gruposAlumnos.fkGrupo, gruposAlumnos.idGruposAlumnos);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int idGrupo)
        {
            //Eliminar
            string consulta = string.Format("Delete from GruposAl where idGA = '{0}'", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<GruposAlumnos> GetGruposAlumnos(string filtro)
        {
            var listGruposAlumnos = new List<GruposAlumnos>();
            var ds = new DataSet();
            string consulta = "Select * from GruposAl where fk_alumno like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "GruposAl");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var gruposAlumnos = new GruposAlumnos
                {
                    idGruposAlumnos = Convert.ToInt32(row["idGA"]),
                    fkGrupo = Convert.ToInt32(row["fk_grupo"]),
                    fkAlumno = Convert.ToInt32(row["fk_alumno"]),
                };
                listGruposAlumnos.Add(gruposAlumnos);

            }
            return listGruposAlumnos;
        }
    }
}
