﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class GruposAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public GruposAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Grupos grupos)
        {
            if (grupos.idGrupo == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into grupos values(null,'{0}','{1}')",grupos.NombreGrupo,grupos.Turno);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update grupos set nombreGrupo = '{0}',turno = '{1}' where idGrupo = {2}", grupos.NombreGrupo,grupos.Turno,grupos.idGrupo);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int idGrupo)
        {
            //Eliminar
            string consulta = string.Format("Delete from grupos where idGrupo = '{0}'", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupos> GetGrupos(string filtro)
        {
            var listGrupo = new List<Grupos>();
            var ds = new DataSet();
            string consulta = "Select * from grupos where nombreGrupo like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var grupos = new Grupos
                {
                    idGrupo = Convert.ToInt32(row["idGrupo"]),
                    NombreGrupo = row["nombreGrupo"].ToString(),
                    Turno = row["turno"].ToString()
                };
                listGrupo.Add(grupos);

            }
            return listGrupo;
        }
    }
}
